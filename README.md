# Description for Delivery API

## Getting Started

Доступные библиотеки для расчета стоимости доставки:

* Edost.ru
    
    Обязательные параметры для запроса:
    * provider_login - Идентификатор магазина из ЛК (http://www.edost.ru/)
    * provider_token - Пароль для доступа к серверу расчетов из ЛК
    * to - Название пунта назначения или цифровой код уникальный для сервиса
    * params[strah] - сумма страховки для отправления
    * weight - вес
    
    Необязательные параметры:
    * delivery_type - Целое число идентификатор способа доставки из личного кабинета (если не указывать выдаст все способы)
    * length, width, height - Габариты отправления (единица измерения выбирается в ЛК)
    * from - Характерно для особых способов доставки иначе не учитывается
    
* Postcalc.ru

    Обязательные параметры для запроса:
    * provider_key - Ключ АПИ из личного кабинета (http://www.postcalc.ru/lk/)
    * to - Почтовый индекс пункта назначения
    * from - Почтовый индекс пункта отправления
    * weight - вес
    
    Необязательные параметр:
    * country - По умолчанию RU (все доступные коды описаны в файле src/Providers/Postcalc/postcalc_light_countries.txt)
    * delivery_type - Конкретный способ доставки, иначе выведет все
        ```
          'RP_Parcel_Cost' => 'ЦеннаяПосылка',
          'RP_Bookpost1C_Cost' => 'ЦеннаяБандероль1Класс',
          'RP_Bookpost1C' => 'ЗаказнаяБандероль1Класс',
          'RP_Bookpost_Cost' => 'ЦеннаяБандероль',
          'RP_Parcel' => 'ЗаказнаяБандероль',
          'RP_Bookpost' => 'ЗаказнаяБандероль',
          'EMS_Courier' => 'EMS',
          'RP_IntlSmallPacket_Terrain' => 'МждМелкийПакет',
          'RP_IntlSmallPacket_Avia' => 'МждМелкийПакетАвиа',
          'RP_Intl_ParcelM_Terrain' => 'МждМешокМ',
          'RP_Intl_ParcelM_Avia' => 'МждМешокМАвиа',
          'RP_IntlParcel_Terrain' => 'МждПосылка',
          'RP_IntlParcel_Avia' => 'МждПосылкаАвиа',
          'EMS_IntlDocs' => 'EMS_МждДокументы',
          'EMS_IntlProducts' => 'EMS_МждТовары',
        ```  

##

* **Evgeniy Petrov** - *Initial work* - [MrKody](https://github.com/MrKody)