<?php


namespace ShopExpress\DeliveryApi;


use ShopExpress\DeliveryApi\Entity\DeliveryType;

/**
 * Class DeliveryResponse
 * @package ShopExpress\DeliveryApi
 */
class DeliveryResponse
{
    /**
     * @var DeliveryType[]
     */
    private $types;

    /**
     * DeliveryResponse constructor.
     *
     * @param DeliveryType[] $types
     */
    public function __construct(array $types)
    {
        $this->types = $types;
    }

    /**
     * @param string|null $id
     *
     * @return DeliveryType[]|DeliveryType
     */
    public function get(string $id = null)
    {
        if (null !== $id) {
            if (!isset($this->types[$id])) {
                throw new \InvalidArgumentException(sprintf('The delivery type with id = `%s` does not exist.', $id));
            }
            return $this->types[$id];
        }
        return $this->types;
    }
}