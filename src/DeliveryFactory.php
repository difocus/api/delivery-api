<?php

namespace ShopExpress\DeliveryApi;

use ShopExpress\DeliveryApi\Providers\DeliveryApiInterface;

/**
 * Class DeliveryFactory
 * @package ShopExpress\DeliveryApi
 */
class DeliveryFactory
{
    /**
     * @var DeliveryFactoryConfiguration
     */
    private $configuration;

    /**
     * DeliveryFactory constructor.
     *
     * @param DeliveryFactoryConfiguration $configuration
     */
    public function __construct(DeliveryFactoryConfiguration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return DeliveryApiInterface
     */
    public function create(): DeliveryApiInterface
    {
        $className = "ShopExpress\\DeliveryApi\\Providers\\{$this->configuration->getProvider()}\\{$this->configuration->getProvider()}Api";
        return new $className($this->configuration);
    }
}