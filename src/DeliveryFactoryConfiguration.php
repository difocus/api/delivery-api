<?php

namespace ShopExpress\DeliveryApi;

use Exception;

/**
 * Class DeliveryFactoryConfiguration
 * @package ShopExpress\DeliveryApi
 */
class DeliveryFactoryConfiguration
{
    public const UNITS_KILOGRAMS = 0;
    public const UNITS_GRAMS = 1;

    /**
     * @var string
     */
    private string $provider;
    /**
     * @var int
     */
    private int $weightUnits;
    /**
     * @var string
     */
    private string $apiToken;
    /**
     * @var float|int
     */
    private float $weight = 0;
    /**
     * @var string|null
     */
    private ?string $to = null;
    /**
     * @var string|null
     */
    private ?string $from = null;
    /**
     * @var string|null
     */
    private ?string $country = null;
    /**
     * @var string|null
     */
    private ?string $region = null;
    /**
     * @var string|null
     */
    private ?string $apiLogin = null;
    /**
     * @var array
     */
    private array $params = [];

    /**
     * DeliveryFactoryConfiguration constructor.
     *
     * @param string $provider
     * @param string $apiToken
     * @param int $weightUnits
     */
    public function __construct(string $provider, string $apiToken, int $weightUnits = self::UNITS_GRAMS)
    {
        $this->provider = $provider;
        $this->apiToken = $apiToken;
        $this->weightUnits = $weightUnits;
    }

    /**
     * @return string
     */
    public function getProvider(): string
    {
        return $this->provider;
    }

    /**
     * @return string|null
     */
    public function getTo(): ?string
    {
        return $this->to;
    }

    /**
     * @param string|null $to
     *
     * @return DeliveryFactoryConfiguration
     */
    public function setTo(?string $to): DeliveryFactoryConfiguration
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        if ($this->weight <= 0) {
            if ($this->weightUnits === self::UNITS_KILOGRAMS) {
                return 0.001;
            }

            return 1;
        }

        return $this->weight;
    }

    /**
     * @param float $weight
     * @return self
     */
    public function setWeight(float $weight): DeliveryFactoryConfiguration
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFrom(): ?string
    {
        return $this->from;
    }

    /**
     * @param string|null $from
     *
     * @return self
     */
    public function setFrom(?string $from): DeliveryFactoryConfiguration
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     *
     * @return self
     */
    public function setCountry(?string $country): DeliveryFactoryConfiguration
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegion(): ?string
    {
        return $this->region;
    }

    /**
     * @param string|null $region
     *
     * @return DeliveryFactoryConfiguration
     */
    public function setRegion(?string $region): DeliveryFactoryConfiguration
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApiLogin(): ?string
    {
        return $this->apiLogin;
    }

    /**
     * @param string|null $apiLogin
     *
     * @return DeliveryFactoryConfiguration
     */
    public function setApiLogin(?string $apiLogin): DeliveryFactoryConfiguration
    {
        $this->apiLogin = $apiLogin;
        return $this;
    }

    /**
     * @return string
     */
    public function getApiToken(): string
    {
        return $this->apiToken;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public function getParam(string $key)
    {
        if (!isset($this->params[$key])) {
            throw new \InvalidArgumentException(sprintf('Param `%s` is not exists', $key));
        }
        return $this->params[$key];
    }

    /**
     * @param string $key
     * @param $value
     * @return self
     */
    public function setParam(string $key, $value): DeliveryFactoryConfiguration
    {
        $this->params[$key] = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getWeightUnits(): int
    {
        return $this->weightUnits;
    }
}