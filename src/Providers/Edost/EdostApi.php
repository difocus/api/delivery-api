<?php


namespace ShopExpress\DeliveryApi\Providers\Edost;


use ShopExpress\DeliveryApi\DeliveryFactoryConfiguration;
use ShopExpress\DeliveryApi\DeliveryResponse;
use ShopExpress\DeliveryApi\Entity\DeliveryType;
use ShopExpress\DeliveryApi\Providers\DeliveryApiInterface;

/**
 * Class EdostApi
 * @package ShopExpress\DeliveryApi\Providers\Edost
 */
class EdostApi implements DeliveryApiInterface
{
    public const EDOST_SERVER = 'api.edost.ru'; // сервер расчета доставки
    public const EDOST_SERVER_ZIP = 'edostzip.ru'; // справочный сервер

    public bool $error = false;
    public array $delimiter = [[',', ':'], [';', '/']];

    /**
     * @var string|null
     */
    private ?string $edostId;
    /**
     * @var string
     */
    private string $edostToken;
    /**
     * @var string|null
     */
    private ?string $city;
    /**
     * @var float
     */
    private float $weight;
    /**
     * @var array
     */
    private array $params;
    /**
     * @var string|null
     */
    private ?string $country;
    /**
     * @var string|null
     */
    private ?string $region;

    /**
     * @inheritDoc
     */
    public function __construct(DeliveryFactoryConfiguration $configuration)
    {
        $this->edostId = $configuration->getApiLogin();
        $this->edostToken = $configuration->getApiToken();
        $this->city = $configuration->getTo();
        $this->region = $configuration->getRegion();
        $this->country = !empty($configuration->getCountry()) ? $configuration->getCountry() : 0;

        $this->weight = $configuration->getWeight();
        if ($configuration->getWeightUnits() === DeliveryFactoryConfiguration::UNITS_GRAMS) {
            $this->weight /= 1000.0; // переводим в килограммы
        }

        $this->params = $configuration->getParams();
    }

    /**
     * @inheritDoc
     */
    public function calculate(): DeliveryResponse
    {
        $this->validate();

        $insurance = $this->params['insurance'] ?? 0;
        $length = $this->params['length'] ?? 0;
        $width = $this->params['width'] ?? 0;
        $height = $this->params['height'] ?? 0;

        $data = [];
        $data[] = 'country=' . $this->country;
        $data[] = 'region=' . $this->region;
        $data[] = 'weight=' . $this->weight;
        $data[] = 'city=' . urlencode($this->utf8_win($this->city));
        $data[] = 'size=' . urlencode(implode('|', [$length, $width, $height]));
        $data[] = 'insurance=' . $insurance;

        if (isset($this->params['zip'])) {
            $data[] = 'zip=' . $this->params['zip'];
        }

        $response = $this->RequestData('', $this->edostId, $this->edostToken, implode('&', $data), 'delivery');

        $types = [];
        if (!empty($response['data'])) {
            foreach ($response['data'] as $item) {
                $types[$item['id']] = new DeliveryType($item['price'] ?? 0, $item);
            }
        }

        if (!empty($types)) {
            // Получаем список пунктов выдачи для доставок
            $offices = $this->GetOffice(array_map(static function (DeliveryType $type) {
                return $type['company_id'];
            }, $types));

            foreach ($types as $key => $type) {
                if (isset($offices[$type['company_id']]) && $type['format'] === 'office') {
                    $types[$key]['office'] = array_values($offices[$type['company_id']]);
                }
            }
        }

        return new DeliveryResponse($types);
    }

    /**
     * @return void
     */
    private function validate(): void
    {
        if (empty($this->edostId) || empty($this->region)) {
            throw new \InvalidArgumentException("Invalid Configuration Settings.");
        }
    }

    /**
     * @param $url
     * @param $id
     * @param $ps
     * @param $post
     * @param $type
     *
     * @return array|false|int[]|string
     */
    private function RequestData($url, $id, $ps, $post, $type)
    {
        if ($type !== 'print') {
            if ($id === '' || $ps === '') {
                return ['error' => 12];
            }
            if ((int)$id == 0) {
                return ['error' => 3];
            }
        }
        if ($post === '') {
            return ['error' => 4];
        }

        $auto = ($url == '');
        $server_type = (in_array($type, ['delivery', 'control', 'detail', 'print']) ? 'main' : 'zip');
        $server_default = ($server_type === 'main' ? self::EDOST_SERVER : self::EDOST_SERVER_ZIP);
        $server = ($auto ? $server_default : $url);
        if ($server == '') {
            $server = $server_default;
        }
        $url = 'http://' . $server . '/' . ($server_type === 'main' ? 'api2.php' : 'api.php');

        if ($type !== 'print') {
            $post = 'id=' . $id . '&p=' . $ps . '&version=2.5.1&' . $post;
        }
        $parse_url = parse_url($url);
        $path = $parse_url['path'];
        $host = $parse_url['host'];

        $this->error = false;
        //set_error_handler(['edost_class', 'RequestError']);

        $fp = fsockopen($host, 80, $errno, $errstr, 4); // 4 - максимальное время запроса
        //restore_error_handler();

        if ($errno == 13 || $this->error || !$fp) {
            $r = ['error' => 14];
        } // настройки сервера не позволяют отправить запрос на расчет
        else {
            $out = "POST " . $path . " HTTP/1.0\r\n" .
                "Host: " . $host . "\r\n" .
                "Referer: " . $url . "\r\n" .
                "Content-Type: application/x-www-form-urlencoded\r\n" .
                "Content-Length: " . strlen($post) . "\r\n\r\n" .
                $post . "\r\n\r\n";

            fwrite($fp, $out);
            $r = '';
            while ($gets = fgets($fp, 512)) {
                $r .= $gets;
            }
            fclose($fp);

            $r = stristr($r, 'api_data:', false);
            if ($r === false) {
                $r = ['error' => 8];
            } // сервер расчета не отвечает
            else {
                $r = substr($r, 9);
                if (!in_array($type, ['develop', 'print'])) {
                    $r = $this->ParseData($r, $type);
                }
            }
        }

        return $r;
    }

    public function ParseData($data, $type = 'delivery')
    {
        if ($type === 'delivery') {
            $key = ['id', 'price', 'priceinfo', 'pricecash', 'priceoffice', 'transfer', 'day', 'insurance', 'company', 'name', 'format', 'company_id', 'priceoriginal'];
        } else if ($type === 'document') {
            $key = ['id', 'data', 'data2', 'name', 'size', 'quantity', 'mode', 'cod', 'delivery', 'length', 'space'];
        } else if ($type === 'office') {
            $key = ['id', 'code', 'name', 'address', 'address2', 'tel', 'schedule', 'gps', 'type', 'metro', 'options'];
        } else if ($type === 'location') {
            $key = ['city', 'region', 'country'];
        } else if ($type === 'location_street') {
            $key = ['street', 'zip', 'city'];
        } else if ($type === 'location_zip') {
            $key = ['zip'];
        } else if ($type === 'location_robot') {
            $key = ['ip_from', 'ip_to'];
        } else if ($type === 'control') {
            $key = ['id', 'flag', 'tariff', 'tracking_code', 'status', 'status_warning', 'status_string', 'status_info', 'status_date', 'status_time', 'day_arrival', 'day_delay', 'day_office', 'register', 'batch'];
        } else if ($type === 'detail') {
            $key = ['status', 'status_warning', 'status_string', 'status_info', 'status_date', 'status_time'];
        } else if ($type === 'tracking') {
            $key = ['id', 'tariff', 'example', 'format'];
        } else if ($type === 'param') {
            $key = [];
        } else {
            return ['error' => 4];
        }

        $r = [];
        $key_count = count($key);
        $data = explode('|', $data);

        // общие параметры: error=2;warning=1;sizetocm=1;...
        $p = explode(';', $data[0]);
        foreach ($p as $v) {
            $s = explode('=', $v);
            $s[0] = preg_replace("/[^0-9_a-z]/i", "", substr($s[0], 0, 20));
            if (isset($s[1]) && $s[0] != '') {
                if ($s[0] === 'limit') {
                    $this->ParseArray($s[1], 'limit', $r);
                } else if ($s[0] === 'field') {
                    $this->ParseArray($s[1], 'field', $r);
                } else if ($s[0] === 'control') {
                    $this->ParseArray($s[1], 'control', $r);
                } else if ($s[0] === 'warning') {
                    $r[$s[0]] = explode(':', $s[1]);
                } else {
                    $r[$s[0]] = $s[1];
                }
            }
        }

        if (isset($r['error']) || $key_count == 0) {
            return $r;
        }

        $r['data'] = [];
        $array_id = '';
        $sort = 0;
        foreach ($data as $k => $v) {
            if ($k == 0 || $v === 'end') {
                if ($k != 0 && isset($parse[$key[0]]) && ($key_count == 1 || isset($parse[$key[1]]))) {
                    $sort++;
                    if ($type === 'delivery') {
                        $profile = $parse['id'] * 2 + ($parse['insurance'] == 1 ? 0 : -1);
                        $parse['profile'] = $profile;
                        $parse['sort'] = $sort * 2;
                        if ($profile > 0) {
                            $r['data'][$profile] = $parse;
                        }
                    } else if ($array_id !== '') {
                        $r['data'][$array_id][$parse['id']] = $parse;
                    } else if (isset($parse['id'])) {
                        $r['data'][$parse['id']] = $parse;
                    } else {
                        $r['data'][] = $parse;
                    }
                }
                $i = 0;
                $parse = [];
            } else if ($v === 'key') {
                $array_id = 'get';
            } else if ($array_id === 'get') {
                $array_id = $v;
            } else if ($i < $key_count) {
                $p = $key[$i];
                $i++;

                if ($type === 'delivery') {
                    if (in_array($p, ['day', 'company', 'name'])) {
                        $v = $this->site_charset(substr($v, 0, 80));
                    } else if (in_array($p, ['price', 'priceinfo', 'pricecash', 'transfer'])) {
                        $v = preg_replace("/[^0-9.-]/i", "", substr($v, 0, 11));
                        if ($v === '') {
                            $v = ($p === 'pricecash' ? -1 : 0);
                        }
                    } else if (in_array($p, ['id', 'insurance'])) {
                        $v = (int)$v;
                    } else if ($p === 'company_id') {
                        $v = preg_replace("/[^a-z0-9]/i", "", substr($v, 0, 3));
                    } else if ($p === 'format') {
                        $v = preg_replace("/[^a-z]/i", "", substr($v, 0, 10));
                    } else if ($p === 'priceoffice') {
                        $this->ParseArray($v, $p, $parse);
                        continue;
                    } else if ($p === 'priceoriginal') {
                        $this->ParseArray($v, $p, $parse);
                        continue;
                    }
                }

                if ($type === 'document') {
                    if ($p === 'insurance' || $p === 'cod') {
                        $v = ($v == 1);
                    } else if ($p === 'delivery') {
                        $v = ($v != '' ? explode(',', $v) : false);
                    } else if ($p === 'size') {
                        $v = explode('x', $v);
                    } else if ($p === 'length' || $p === 'space') {
                        $v = explode(',', $v);
                        $o = [];
                        foreach ($v as $s) {
                            if ($s != '') {
                                $s = explode('=', $s);
                                if ($s[0] != '') {
                                    $o[$s[0]] = (isset($s[1]) ? (int)$s[1] : 0);
                                }
                            }
                        }
                        $v = $o;
                    }
                }

                if ($type === 'office') {
                    if ($p === 'type') {
                        $v = (int)$v;
                    } else if (in_array($p, ['id', 'gps'])) {
                        $v = preg_replace("/[^a-z0-9.,]/i", "", substr($v, 0, 30));
                    } else {
                        $v = $this->site_charset(trim(substr($v, 0, 160)));
                    }
                }

                if ($type === 'location') {
                    if ($p === 'country' || $p === 'region') {
                        $v = (int)$v;
                    } else {
                        $v = $this->site_charset(substr($v, 0, 160));
                    }
                }

                if ($type === 'location_street') {
                    if (in_array($p, ['street', 'city'])) {
                        $v = $this->site_charset(substr($v, 0, 160));
                    }
                }

                if ($type === 'location_zip') {
                    $v = preg_replace("/[^0-9]/i", "", substr($v, 0, 6));
                }

                if ($type === 'location_robot') {
                    $v = preg_replace("/[^0-9.]/i", "", substr($v, 0, 15));
                }

                if ($type === 'control' || $type === 'detail') {
                    if (in_array($p, ['id', 'flag', 'status', 'tariff', 'status_warning', 'day_arrival', 'day_delay', 'day_office'])) {
                        $v = (int)$v;
                    } /*else if ($p === 'batch') {
                    $v = $this->UnPackDataArray($v, 'batch');
                    if (!empty($v['date']) && !empty($v['number'])) {
                        $parse['batch_code'] = $v['date'] . '_' . $v['number'];
                    }
                }*/ else {
                        $v = $this->site_charset(substr($v, 0, 500));
                    }
                }

                if ($type === 'tracking') {
                    if ($p === 'company_id') {
                        $v = (int)$v;
                    } else if ($p === 'tariff') {
                        $v = explode(',', $v);
                    } else {
                        $v = $this->site_charset(substr($v, 0, 500));
                    }
                }

                $parse[$p] = $v;
            }
        }

        return $r;
    }

    public function ParseArray($array, $id, &$data, $level = 0)
    {
        if (in_array($id, ['field', 'control'])) {
            $array = $this->site_charset(substr($array, 0, 10000));
        } else {
            $array = preg_replace("/[^0-9.:,;-]/i", "", substr($array, 0, 1000));
        }
        if ($array == '') {
            return;
        }

        if ($id === 'priceoffice') {
            $key = ['type', 'price', 'priceinfo', 'pricecash', 'priceoriginal'];
        } else if ($id === 'priceoriginal') {
            $key = ['price', 'pricecash'];
        } else if ($id === 'limit') {
            $key = ['company_id', 'type', 'weight_from', 'weight_to', 'price', 'size1', 'size2', 'size3', 'sizesum'];
        } else if ($id === 'field') {
            $key = ['name', 'value'];
        } else if ($id === 'control') {
            $key = ['id', 'count', 'site'];
        } else {
            return;
        }

        $key_count = count($key);
        $default = array_fill_keys($key, 0);
        if ($id === 'priceoffice') {
            $default['pricecash'] = -1;
            unset($default['priceoriginal']);
        }
        if ($id === 'priceoriginal') {
            unset($default['pricecash']);
        }

        $r = [];
        $delimiter = $this->delimiter[$level];
        $array = explode($delimiter[1], $array);
        foreach ($array as $v) {
            $v = explode($delimiter[0], $v);
            if ($v[0] == '' || (!isset($v[1]) && $id !== 'priceoriginal')) {
                continue;
            }

            $ar = $default;
            foreach ($v as $k2 => $v2) {
                if ($k2 < $key_count && $v2 !== '') {
                    if ($key[$k2] === 'priceoriginal') {
                        $this->ParseArray($v2, $key[$k2], $ar, 1);
                    } else {
                        $ar[$key[$k2]] = str_replace(['%c', '%t'], [',', ':'], $v2);
                    }
                }
            }

            if ($id === 'priceoriginal') {
                $r = $ar;
            } else if (in_array($id, ['priceoffice', 'control'])) {
                $r[$v[0]] = $ar;
            } else {
                $r[] = $ar;
            }
        }
        if (!empty($r)) {
            $data[$id] = $r;
        }
    }

    public function site_charset($s, $charset = '')
    {
        $utf = true;
        if ($charset === 'utf' && !$utf) {
            $s = $this->utf8_win($s);
        } else if ($charset !== 'utf' && $utf) {
            $s = $this->win_utf8($s);
        }
        return $s;
    }

    public function win_utf8($s)
    {
        $utf = '';
        $mb = function_exists('mb_substr');
        $n = (function_exists('mb_strlen') ? mb_strlen($s, 'windows-1251') : strlen($s));
        for ($i = 0; $i < $n; $i++) {
            $donotrecode = false;
            $c = ord($mb ? mb_substr($s, $i, 1, 'windows-1251') : substr($s, $i, 1));
            if ($c == 0xA8) {
                $res = 0xD081;
            } elseif ($c == 0xB8) {
                $res = 0xD191;
            } elseif ($c < 0xC0) {
                $donotrecode = true;
            } elseif ($c < 0xF0) {
                $res = $c + 0xCFD0;
            } else {
                $res = $c + 0xD090;
            }
            $c = ($donotrecode) ? chr($c) : (chr($res >> 8) . chr($res & 0xff));
            $utf .= $c;
        }
        return $utf;
    }

    public function utf8_win($s)
    {
        $out = '';
        $c1 = '';
        $byte2 = false;
        $n = (function_exists('mb_strlen') ? mb_strlen($s, 'windows-1251') : strlen($s));
        for ($c = 0; $c < $n; $c++) {
            $i = ord($s[$c]);
            if ($i <= 127) {
                $out .= $s[$c];
            }
            if ($byte2) {
                $new_c2 = ($c1 & 3) * 64 + ($i & 63);
                $new_c1 = ($c1 >> 2) & 5;
                $new_i = $new_c1 * 256 + $new_c2;
                if ($new_i == 1025) {
                    $out_i = 168;
                } else {
                    if ($new_i == 1105) {
                        $out_i = 184;
                    } else {
                        $out_i = $new_i - 848;
                    }
                }
                $out .= chr($out_i);
                $byte2 = false;
            }
            if (($i >> 5) == 6) {
                $c1 = $i;
                $byte2 = true;
            }
        }
        return $out;
    }

    public function GetOffice($company)
    {
        if (empty($company)) {
            return false;
        }

        $company = implode(',', $company);

        $data = [];
        $data[] = 'type=office';
        $data[] = 'country=' . $this->country;
        $data[] = 'region=' . $this->region;
        $data[] = 'city=' . urlencode($this->utf8_win($this->city));
        $data[] = 'company=' . urlencode($company);

        $response = $this->RequestData('', $this->edostId, $this->edostToken, implode('&', $data), 'office');

        // ограничение по параметрам заказа
        if (!empty($response['data']) && !empty($response['limit'])) {
            foreach ($response['limit'] as $v) {
                if (isset($response['data'][$v['company_id']])) {
                    foreach ($response['data'][$v['company_id']] as $k2 => $v2) {
                        if ($v2['type'] == $v['type']) {
                            $a = false;
                            if ($this->weight < $v['weight_from'] || ($v['weight_to'] != 0 && $this->weight > $v['weight_to'])) {
                                $a = true;
                            }

                            // @TODO
                            /*$data = ['size1', 'size2', 'size3', 'sizesum'];
                            foreach ($response as $s) {
                                if ($v[$s] != 0 && $order[$s] > $v[$s]) {
                                    $a = true;
                                }
                            }*/

                            if ($a) {
                                unset($response['data'][$v['company_id']][$k2]);
                            }

                            // @TODO
                            /*else if ($v['price'] != 0) {
                                $data['data'][$v['company_id']][$k2]['codmax'] = (int)($v['price'] - $order['price'] - 1);
                            }*/
                        }
                    }
                }
            }
        }

        return $response['data'];
    }
}