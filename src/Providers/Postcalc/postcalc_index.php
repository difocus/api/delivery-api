<?php
//error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
// Загружаем библиотеку
require_once 'postcalc_light_lib.php';
extract($arrPostcalcConfig, EXTR_PREFIX_ALL, 'postcalc_config');
// Инициализируем значения полей формы
$postcalc_from = ( isset($_GET['index1']) ) ? $_GET['index1'] : $postcalc_config_default_from;
$postcalc_to = ( isset($_GET['index2']) ) ? $_GET['index2'] : false;
$postcalc_weight = ( isset($_GET['weight'])) ? $_GET['weight'] : false;
$postcalc_valuation = 0;
$postcalc_country = ( isset($_GET['country']) ) ? $_GET['country'] : 'RU';
$delivery_type = ( isset($_GET['type']) ) ? $_GET['type'] : false;

// Выдаем заголовок с указанием на кодировку
$header = "Content-Type: %s; charset=$postcalc_config_cs";

if ($postcalc_from && $postcalc_to && $postcalc_weight && $delivery_type) {
    // Обращаемся к функции getPostcalc
    $arrResponse = postcalc_request(
        $postcalc_from,
        $postcalc_to,
        $postcalc_weight,
        $postcalc_valuation,
        $postcalc_country
    );

    // Если вернулась строка - это сообщение об ошибке.
    if (!is_array($arrResponse)) {
        if (error_get_last() && count(error_get_last())) {
            $arrError = error_get_last();
            $response = json_encode(['success' => false, 'message' => "Ошибка PHP, строка $arrError[line] в файле $arrError[file]: $arrError[message]", 'price' => 0]);
        } else {
            $response = json_encode(['success' => false, 'message' => $arrResponse, 'price' => 0]);
        }
    } else {
        if ($postcalc_country == 'RU') {
            switch ($delivery_type) {
                case 'RP_Parcel_Cost':
                    $price = $arrResponse['Отправления']['ЦеннаяПосылка']['Доставка'];
                    break;
                case 'RP_Bookpost1C_Cost':
                    $price = $arrResponse['Отправления']['ЦеннаяБандероль1Класс']['Доставка'];
                    break;
                case 'RP_Bookpost1C':
                    $price = $arrResponse['Отправления']['ЗаказнаяБандероль1Класс']['Доставка'];
                    break;
                case 'RP_Bookpost_Cost':
                    $price = $arrResponse['Отправления']['ЦеннаяБандероль']['Доставка'];
                    break;
                case 'RP_Parcel':
                case 'RP_Bookpost':
                    $price = $arrResponse['Отправления']['ЗаказнаяБандероль']['Доставка'];
                    break;
                case 'EMS_Courier':
                    $price = $arrResponse['Отправления']['EMS']['Доставка'];
                    break;
                default:
                    $price = 0;
            }
        } else {
            switch ($delivery_type) {
                case 'RP_IntlSmallPacket_Terrain':
                    $price = $arrResponse['Отправления']['МждМелкийПакет']['Доставка'];
                    break;
                case 'RP_IntlSmallPacket_Avia':
                    $price = $arrResponse['Отправления']['МждМелкийПакетАвиа']['Доставка'];
                    break;
                case 'RP_Intl_ParcelM_Terrain':
                    $price = $arrResponse['Отправления']['МждМешокМ']['Доставка'];
                    break;
                case 'RP_Intl_ParcelM_Avia':
                    $price = $arrResponse['Отправления']['МждМешокМАвиа']['Доставка'];
                    break;
                case 'RP_IntlParcel_Terrain':
                    $price = $arrResponse['Отправления']['МждПосылка']['Доставка'];
                    break;
                case 'RP_IntlParcel_Avia':
                    $price = $arrResponse['Отправления']['МждПосылкаАвиа']['Доставка'];
                    break;
                case 'EMS_IntlDocs':
                    $price = $arrResponse['Отправления']['EMS_МждДокументы']['Доставка'];
                    break;
                case 'EMS_IntlProducts':
                    $price = $arrResponse['Отправления']['EMS_МждТовары']['Доставка'];
                    break;
                default:
                    $price = 0;
            }
        }

        if (isset($_GET['_debug'])) {
            print_r($arrResponse);
        }

        $response = json_encode(['success' => true, 'price' => $price]);
    }
} else {
    $response = json_encode(['success' => false, 'price' => 0]);
}

if (isset($_GET['callback']) && !empty($callback = $_GET['callback'])) {
    header(sprintf($header, 'application/javascript'));
    echo "$callback($response)";
} else {
    header(sprintf($header, 'application/json'));
    echo $response;
}
