<?php

namespace ShopExpress\DeliveryApi\Providers\Postcalc;

use Exception;
use ShopExpress\DeliveryApi\DeliveryFactoryConfiguration;
use ShopExpress\DeliveryApi\DeliveryResponse;
use ShopExpress\DeliveryApi\Entity\DeliveryType;
use ShopExpress\DeliveryApi\Providers\DeliveryApiInterface;


/**
 * Class PostcalcApi
 * @package ShopExpress\DeliveryApi\Providers\Postcalc
 */
class PostcalcApi implements DeliveryApiInterface
{
    /**
     * @var mixed
     */
    private $postcalcFrom;
    /**
     * @var string
     */
    private $postcalcTo;
    /**
     * @var int
     */
    private $postcalcWeight;
    /**
     * @var int
     */
    private $postcalcValuation;
    /**
     * @var mixed|string
     */
    private $postcalcCountry;
    /**
     * @var string
     */
    private $postcalcKey;

    /**
     * @var array
     */
    private $deliveryTypesMapping = [
        'Посылка1Класс' => 'RP_Parcel1Class',
        'ЦеннаяПосылка' => 'RP_Parcel_Cost',
        'ЦеннаяБандероль1Класс' => 'RP_Bookpost1C_Cost',
        'ЗаказнаяБандероль1Класс' => 'RP_Bookpost1C',
        'ЦеннаяБандероль' => 'RP_Bookpost_Cost',
        'ПростаяБандероль' => 'RP_BookpostSimple',
        'ЗаказнаяБандероль' => 'RP_Bookpost',
        'EMS' => 'EMS_Courier',
        'МждМелкийПакет' => 'RP_IntlSmallPacket_Terrain',
        'МждМелкийПакетАвиа' => 'RP_IntlSmallPacket_Avia',
        'МждМешокМ' => 'RP_Intl_ParcelM_Terrain',
        'МждМешокМАвиа' => 'RP_Intl_ParcelM_Avia',
        'МждМешокМЗаказной' => 'RP_IntParcelMReg',
        'МждМешокМАвиаЗаказной' => 'RP_IntParcelMAviaReg',
        'МждМелкийПакетЗаказной' => 'RP_IntSmallPacketReg',
        'МждПосылка' => 'RP_IntlParcel_Terrain',
        'МждПосылкаАвиа' => 'RP_IntlParcel_Avia',
        'EMS_МждДокументы' => 'EMS_IntlDocs',
        'EMS_МждТовары' => 'EMS_IntlProducts',
    ];

    /**
     * @var array
     */
    private $fieldsForResponse = [
        'Название' => 'name',
        'Доставка' => 'price',
        'СрокДоставки' => 'days',
    ];

    /**
     * DeliveryApiInterface constructor.
     * @param DeliveryFactoryConfiguration $configuration
     */
    public function __construct(DeliveryFactoryConfiguration $configuration)
    {
        $this->postcalcKey = $configuration->getApiToken();
        $this->postcalcFrom = $configuration->getFrom();
        $this->postcalcTo = $configuration->getTo();

        $this->postcalcWeight = $configuration->getWeight();
        if ($configuration->getWeightUnits() === DeliveryFactoryConfiguration::UNITS_KILOGRAMS) {
            $this->postcalcWeight *= 1000; // переводим в граммы
        }

        $this->postcalcValuation = 0;
        $this->postcalcCountry = $configuration->getCountry() ?? 'RU';
    }

    /**
     * @throws Exception
     * @return DeliveryResponse
     */
    public function calculate(): DeliveryResponse
    {
        $this->validate();

        $response = $this->sendRequest();

        $types = [];
        if (!empty($response['Отправления'])) {
            foreach ($response['Отправления'] as $key => $item) {
                if (!empty($deliveryType = $this->deliveryTypesMapping[$key])) {
                    $types[$deliveryType] = $this->retrieveDeliveryType($item);
                }
            }
        }

        return new DeliveryResponse($types);
    }

    /**
     * @return void
     */
    private function validate(): void
    {
        if (empty($this->postcalcFrom) && empty($this->postcalcTo)) {
            throw new \InvalidArgumentException('Invalid Configuration Settings.');
        }
    }

    /**
     * @param array $content
     *
     * @return DeliveryType
     */
    private function retrieveDeliveryType(array $content): DeliveryType
    {
        $fields = [];
        foreach ($this->fieldsForResponse as $key => $value) {
            if (isset($content[$key])) {
                $fields[$value] = $content[$key];
            }
        }

        return new DeliveryType($fields['price'] ?? 0, $fields);
    }

    /**
     * @return array
     * @throws Exception
     */
    private function sendRequest(): array
    {
        $response = postcalc_request(
            $this->postcalcFrom,
            $this->postcalcTo,
            $this->postcalcWeight,
            $this->postcalcValuation,
            $this->postcalcCountry,
            $this->postcalcKey
        );

        // Если вернулась строка - это сообщение об ошибке.
        if (!is_array($response)) {
            if (error_get_last() && count(error_get_last())) {
                $arrError = error_get_last();
                throw new \RuntimeException("Ошибка PHP, строка $arrError[line] в файле $arrError[file]: $arrError[message]");
            }

            throw new \RuntimeException($response);
        }

        return $response;
    }
}