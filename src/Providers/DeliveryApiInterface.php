<?php

namespace ShopExpress\DeliveryApi\Providers;

use ShopExpress\DeliveryApi\DeliveryFactoryConfiguration;
use ShopExpress\DeliveryApi\DeliveryResponse;


/**
 * Interface DeliveryApiInterface
 * @package ShopExpress\DeliveryApi\Providers
 */
interface DeliveryApiInterface
{
    /**
     * DeliveryApiInterface constructor.
     * @param DeliveryFactoryConfiguration $configuration
     */
    public function __construct(DeliveryFactoryConfiguration $configuration);

    /**
     * @return DeliveryResponse
     */
    public function calculate(): DeliveryResponse;
}