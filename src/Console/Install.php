<?php

namespace ShopExpress\DeliveryApi\Console;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

class Install
{
    /**
     * @param Event $event
     */
    public static function postInstall(Event $event)
    {
        $config = '';
        $io = $event->getIO();

        $arg = $io->ask('Enter api token (or leave empty for generate): ');
        $config .= 'API_TOKEN=' . (escapeshellcmd($arg)? escapeshellcmd($arg): md5(microtime(true)));
        $config .= "\n";

        file_put_contents('.env', $config);

        $arg = $io->askConfirmation('Copy "example/index.php" file to root directory (yes/no, default - yes)? ', true);
        if (escapeshellcmd($arg)) {
            try {
                copy('example/index.php', 'index.php');
            } catch (IOException $e) {
                throw new \InvalidArgumentException('<error>Could not copy "example/index.php"</error>');
            }
        } else {
            $io->write('File "example/index.php" wasn\'t copied');
        }

        $io->write('Delivery Api Successfully installed!');
    }
}