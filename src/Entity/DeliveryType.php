<?php


namespace ShopExpress\DeliveryApi\Entity;


/**
 * Class DeliveryType
 * @package ShopExpress\DeliveryApi\Entity
 */
class DeliveryType implements \ArrayAccess
{
    /**
     * @var float
     */
    private $price;
    /**
     * @var array
     */
    private $data;

    /**
     * DeliveryType constructor.
     *
     * @param float $price
     * @param array $data
     */
    public function __construct(float $price, array $data = [])
    {
        $this->price = $price;
        $this->data = $data;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset): bool
    {
        return isset($this->data[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->data[$offset] : null;
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value): void
    {
        $this->data[$offset] = $value;
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset): void
    {
        unset($this->data[$offset]);
    }
}