<?php

if (!file_exists(__DIR__.'/.env')) {
    exit(".env file does not exists. Please, run composer install command.");
}

$config = parse_ini_file(__DIR__.'/.env');

require 'vendor/autoload.php';

use ShopExpress\DeliveryApi\DeliveryFactory;
use ShopExpress\DeliveryApi\DeliveryFactoryConfiguration;


function response(string $data): string
{
    if (isset($_GET['callback']) && !empty($callback = $_GET['callback'])) {
        header("Content-Type: application/javascript; charset=utf-8");
        return "$callback($data)";
    }

    header("Content-Type: application/json; charset=utf-8");
    return $data;
}

try {
    $env = parse_ini_file(__DIR__.'/.env');

    if (@$_GET['token'] != $env['API_TOKEN']) {
        throw new Exception('Invalid API token!', 401);
    }

    if (!isset($_GET['provider'], $_GET['city'], $_GET['delivery_method'], $_GET['provider_login'], $_GET['provider_token'])) {
        throw new Exception('Not all required parameters are specified.');
    }

    $config = (new DeliveryFactoryConfiguration($_GET['provider'], $_GET['provider_token']))
        ->setApiLogin($_GET['provider_login']);

    if (isset($_GET['city'])) {
        $config->setTo($_GET['city']);
    }
    if (isset($_GET['weight'])) {
        $config->setWeight($_GET['weight']);
    }
    if (isset($_GET['country'])) {
        $config->setCountry($_GET['country']);
    }
    if (isset($_GET['region'])) {
        $config->setRegion($_GET['region']);
    }
    if (isset($_GET['from'])) {
        $config->setFrom($_GET['from']);
    }
    if (isset($_GET['params'])) {
        foreach ($_GET['params'] as $key => $param) {
            $config->setParam($key, $param);
        }
    }

    $delivery = (new DeliveryFactory($config))->create();
    $deliveryResponse = $delivery->calculate();

    echo response(json_encode([
        'success' => true,
        'data' => $deliveryResponse->get($_GET['delivery_method'])->getPrice()
    ], JSON_THROW_ON_ERROR));
} catch (\Throwable $e) {
    echo response(json_encode([
        'success' => false,
        'message' => $e->getMessage()
    ], JSON_THROW_ON_ERROR));
}